public class Cartridge extends Medium{
    private boolean vollstaedigesSet;
    private String entwicklerstudio;
    private Konsolentyp konsolentyp;

    public Cartridge(String titel, double wert, String standort, boolean vollstaedigesSet, String entwicklerstudio, Konsolentyp konsolentyp) {
        super(titel, wert, standort);
        this.vollstaedigesSet = vollstaedigesSet;
        this.entwicklerstudio = entwicklerstudio;
        this.konsolentyp = konsolentyp;
    }

    public boolean isVollstaedigesSet() {
        return vollstaedigesSet;
    }

    public void setVollstaedigesSet(boolean vollstädigesSet) {
        this.vollstaedigesSet = vollstädigesSet;
    }

    public String getEntwicklerstudio() {
        return entwicklerstudio;
    }

    public void setEntwicklerstudio(String entwicklerstudio) {
        this.entwicklerstudio = entwicklerstudio;
    }

    public Konsolentyp getKonsolentyp() {
        return konsolentyp;
    }

    public void setKonsolentyp(Konsolentyp konsolentyp) {
        this.konsolentyp = konsolentyp;
    }

    @Override
    public void anzeigen(){
        super.anzeigen();
        System.out.println("Cartridge: Ist das Set vollständig? -> " + this.isVollstaedigesSet() + ", Entwicklerstudio -> " + this.getEntwicklerstudio() + ", Konsolentyp -> " + this.getKonsolentyp());
    }
}
