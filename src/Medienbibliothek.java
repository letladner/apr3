import java.util.ArrayList;

public class Medienbibliothek {
    private ArrayList<Medium> mediumListe;

    public Medienbibliothek(){
        this.mediumListe = new ArrayList<>();
    }

    public void mediumHinzufuegen(Medium m){
        this.mediumListe.add(m);
    }

    public void alleMedienAusgeben(){
        for (Medium m : mediumListe){
            m.anzeigen();
        }
    }

    public ArrayList<VHS> alleVHSalsListe(){
        ArrayList<VHS> vhs = new ArrayList<>();
        for (Medium m : mediumListe){
            if (m instanceof VHS){
                vhs.add((VHS)m);
            }
        }
        return vhs;
    }
}
